const axios = require('axios')
const fs = require('fs')

const fetchFollowers = async (cursor = -1, userName) => {
    console.log('api called')
    const response = await axios.get(
        'https://api.twitter.com/1.1/followers/list.json',
        {
            params: {
                cursor: cursor,
                screen_name: userName,
                skip_status: 'true',
                include_user_entities: 'false',
                with_total_count: 'true',
                count: '200',
            },
            headers: {
                authority: 'twitter.com',
                accept: '*/*',
                'accept-language': 'en-IN,en;q=0.9',
                authorization:
                    'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA',
                cookie: 'guest_id=v1%3A168440675573138592; kdt=3OUmyo0XGiKlqOxcjug5V5hof4hwWITOPBIFrc0L; auth_token=c305bcfcd03f926e1c73e47189a688b780c6456a; ct0=7ed7825529d33ee786c42f2b398d38f31b421bbf70705ed164b3bd8aa77481125bd6a5a809f041f07de9ece3fc62baf555b265f6513f466d8e5e072c882672a671b6615f134f7f54d78a95227cb8cd14; twid=u%3D1721168389; guest_id_marketing=v1%3A168440675573138592; guest_id_ads=v1%3A168440675573138592; external_referer=8e8t2xd8A2w%3D|0|Row7tAmsAfIEmjPNOVSmwU7WBxyUUJED%2FJi%2FFP0ccJSNwbn7WrvXeX6xpEMMG6KK; _ga=GA1.2.1801532343.1687784705; _gid=GA1.2.282549072.1687784705; des_opt_in=Y; mbox=PC#f71a35eaac6c431e81edb5fabedd7453.31_0#1751046053|session#8ed442e2c3e44c8581c9aa66da562788#1687803113; lang=en; personalization_id="v1_CE/Siiq/fLsVe5kk5athDw=="; ct0=96c3ae0610c10c878d2992cbf63cacbbce704a75f016851c871d33ded12f0003bd6d5b442ea888eab2fa2a90743360dad58076bf676711cf25e0af4dbc9cb02d5f2a0c30677ede33810b5b1c6b0b8cab; guest_id=v1%3A168780101671461245; guest_id_ads=v1%3A168780101671461245; guest_id_marketing=v1%3A168780101671461245; personalization_id="v1_qyzGj1dY6/3ncMI09CHzVg=="',
                referer: 'https://twitter.com/halalhomer_/followers',
                'sec-ch-ua':
                    '"Not.A/Brand";v="8", "Chromium";v="114", "Brave";v="114"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
                'sec-gpc': '1',
                'user-agent':
                    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
                'x-client-uuid': 'ae800003-31d6-4858-b5ec-8ef65942edca',
                'x-csrf-token':
                    '7ed7825529d33ee786c42f2b398d38f31b421bbf70705ed164b3bd8aa77481125bd6a5a809f041f07de9ece3fc62baf555b265f6513f466d8e5e072c882672a671b6615f134f7f54d78a95227cb8cd14',
                'x-twitter-active-user': 'yes',
                'x-twitter-auth-type': 'OAuth2Session',
                'x-twitter-client-language': 'en',
            },
        }
    )

    return response.data
}

// while (true) {
//     let breaks = false
//     let cursor = -1
//     fetchFollowers(cursor).then((response) => {
//         for (const follower of response.users) {
//             console.log(follower.name)
//         }
//         if (response.next_cursor == 0) {
//             breaks = true
//         }
//         cursor = response.next_cursor
//     })
//     if (breaks) {
//         break
//     }
// }

const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms))
}

const fetchs = async () => {
    let breaks = false
    let cursor = -1
    let count = 0
    const userName = 'shad_rais'
    fs.writeFileSync('followers.csv', 'name,screen_name;\n')
    while (true) {
        const response = await fetchFollowers(cursor, userName)
        await sleep(3000)
        for (const follower of response.users) {
            count++
            console.log(count)
            console.log(follower.name)
            fs.appendFileSync(
                'followers.csv',
                follower.name + ',' + follower.screen_name + ';\n'
            )
        }
        if (response.next_cursor == 0) {
            breaks = true
        }
        cursor = response.next_cursor
        if (breaks) {
            console.log('done', {
                count: count,
                cursor: cursor,
            })
            break
        }
    }
}

fetchs()
